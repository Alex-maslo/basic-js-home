// Знайти всі параграфи на сторінці та встановити колір фону #ff0000

let paragraph = document.querySelectorAll("p");

for (element of paragraph) {
  element.style.background = "#ff0000";
  element.style.opacity = "1";
}

// Знайти елемент із id="optionsList". Вивести у консоль.
// Знайти батьківський елемент та вивести в консоль.
// Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

let optionsList = document.getElementById("optionsList");
console.log(optionsList);

console.log(optionsList.parentNode);
console.log(optionsList.childNodes);

// Встановіть в якості контента елемента з класом testParagraph
// наступний параграф - This is a paragraph

document.querySelector("#testParagraph").innerHTML = "This is a paragraph";

// Отримати елементи, вкладені в елемент із класом main-header
// і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

let mainHeader = document.querySelectorAll(".main-header *");

mainHeader.forEach((elements) => {
  elements.classList.add("nav-item");
  console.log(elements);
});

// Знайти всі елементи із класом section-title.
// Видалити цей клас у цих елементів.

let sectionTitle = document.querySelectorAll(".section-title");
for (elem of sectionTitle) {
  elem.classList.remove("section-title");
}
