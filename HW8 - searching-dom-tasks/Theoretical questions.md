# Опишіть своїми словами що таке Document Object Model (DOM)

Специфікація, яка представляє весь контент сторінки як об’єкти, які можуть бути змінені;

# Яка різниця між властивостями HTML-елементів innerHTML та innerText?

Властивість innerText виводить тільки текст елементу, без HTML-тегів, innerHTML виводить всю структуру елементу з вкладеними тегами;

# Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

document.getElementById();
document.getElementsByName();
document.getElementsByTagName();
document.getElementsByClassName();
document.querySelector();
document.querySelectorAll();

До елемента сторінки можна звернутися за id, name, tag name,
class name та назвою CSS селектора.

Зручніше звертатися за назвою CSS селектора.
