const image = document.querySelectorAll(".image-to-show");

let i = 1;

function showImage() {
  image.forEach((e) => {
    e.classList.remove("active");
  });
  image[i].classList.add("active");
  i++;

  if (i == image.length) {
    i = 0;
  }
}

let imgHide = setInterval(showImage, 1000);

function stop() {
  return clearInterval(imgHide);
}

function run() {
  clearInterval(imgHide);
  return (imgHide = setInterval(showImage, 1000));
}

setTimeout(() => {
  let button = document.querySelectorAll(".btn");
  button.forEach((e) => {
    e.classList.add("active");
  });
}, 2500);
