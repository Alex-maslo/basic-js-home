"use strict";

let userAge, userName;

do {
  userAge = +prompt("How old are you?\nYou must enter the number");
} while (isNaN(userAge) == true);

do {
  userName = prompt("What is your name?\nYou must enter the text");
  if (userName === null) {
    break;
  }
} while (isNaN(userName) == false);

if (userAge < 18) {
  alert("You are not allowed to visit this website");
} else if (userAge >= 18 && userAge <= 22) {
  confirm("Are you sure you want to continue?") == true
    ? alert(`Welcome, ${userName}`)
    : alert("You are not allowed to visit this website");
} else {
  alert(`Welcome, ${userName}`);
}
