# Чому для роботи з input не рекомендується використовувати клавіатуру?

Це ненадійно, тому що введення даних не обов'язково може здійснюватися за допомогою клавіатури. Події клавіатури повинні використовуватися тільки за призначенням – для клавіатури. Наприклад, щоб реагувати на гарячі або спеціальні кнопки.
