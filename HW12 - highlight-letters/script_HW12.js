let btnEnter = document.querySelector(".btn:nth-child(1)");
let btnS = document.querySelector(".btn:nth-child(2)");
let btnE = document.querySelector(".btn:nth-child(3)");
let btnO = document.querySelector(".btn:nth-child(4)");
let btnN = document.querySelector(".btn:nth-child(5)");
let btnL = document.querySelector(".btn:nth-child(6)");
let btnZ = document.querySelector(".btn:nth-child(7)");

document.addEventListener("keypress", function (event) {
  event.code == "Enter"
    ? (btnEnter.style.background = "blue")
    : (btnEnter.style.background = "");

  event.code == "KeyS"
    ? (btnS.style.background = "blue")
    : (btnS.style.background = "");

  event.code == "KeyE"
    ? (btnE.style.background = "blue")
    : (btnE.style.background = "");

  event.code == "KeyO"
    ? (btnO.style.background = "blue")
    : (btnO.style.background = "");

  event.code == "KeyN"
    ? (btnN.style.background = "blue")
    : (btnN.style.background = "");

  event.code == "KeyL"
    ? (btnL.style.background = "blue")
    : (btnL.style.background = "");

  event.code == "KeyZ"
    ? (btnZ.style.background = "blue")
    : (btnZ.style.background = "");
});
