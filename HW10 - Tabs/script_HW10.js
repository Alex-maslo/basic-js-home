function openCity(evt, activeTab) {
  // Declare all variables

  // Get all elements with class="tabcontent" and hide them
  let tabcontent = document.querySelectorAll(".tabcontent");
  for (element of tabcontent) {
    element.style.display = "none";
  }

  // Get all elements with class="tablinks" and remove the class "active"

  let tabTitle = document.querySelectorAll(".tabs-title");
  for (element of tabTitle) {
    element.className = element.className.replace("active", "");
  }

  // Show the current tab, and add an "active" class to the button that opened the tab
  document.getElementById(activeTab).style.display = "block";
  evt.currentTarget.className += " active";
}

document.getElementById("defaultOpen").click();
