"use strict";

/* Реалізувати функцію, яка виконуватиме математичні
операції з введеними користувачем числами.
Завдання має бути виконане на чистому Javascript
без використання бібліотек типу jQuery або React.

Технічні вимоги:
Отримати за допомогою модального вікна браузера два числа.
Отримати за допомогою модального вікна браузера
математичну операцію, яку потрібно виконати.
Сюди може бути введено +, -, *, /.
Створити функцію, в яку передати два значення та операцію.
Вивести у консоль результат виконання функції.
Необов'язкове завдання підвищеної складності
Після введення даних додати перевірку їхньої коректності.
Якщо користувач не ввів числа, або при вводі вказав не
числа, - запитати обидва числа знову
(при цьому значенням за замовчуванням для кожної зі
змінних має бути введена інформація раніше).*/

let firstNumber;
let secondNumber;
let operation;

do {
  firstNumber = +prompt("Введіть перше число:");
  secondNumber = +prompt("Введіть друге число:");

  if (isNaN(firstNumber) == true || isNaN(secondNumber) == true) {
    alert("Потрібно ввести число");
  }
} while (isNaN(firstNumber) == true || isNaN(secondNumber) == true);

do {
  operation = prompt("Введіть знак математичної операції");

  if (
    operation != "+" &&
    operation != "-" &&
    operation != "*" &&
    operation != "/"
  ) {
    alert("Сюди може бути введено +, -, *, /.");
  }
} while (
  operation != "+" &&
  operation != "-" &&
  operation != "*" &&
  operation != "/"
);

function calculator(firstNumber, secondNumber, operation) {
  switch (operation) {
    case "+":
      return firstNumber + secondNumber;
      break;

    case "-":
      return firstNumber - secondNumber;
      break;

    case "*":
      return firstNumber * secondNumber;
      break;

    case "/":
      if (secondNumber == 0) {
        alert("Ділити на нуль заборонено");
      } else return firstNumber / secondNumber;
      break;

    default:
      break;
  }
}

console.log(calculator(firstNumber, secondNumber, operation));
