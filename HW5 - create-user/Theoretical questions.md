# Опишіть своїми словами, що таке метод об'єкту

Метод - це функція, яка працює з даним об'єктом;

# Який тип даних може мати значення властивості об'єкта?

Значення може бути будь-яким;

# Об'єкт це посилальний тип даних. Що означає це поняття?

Це означає, що змінна посилається на якесь значення, а не зберігає
фактичне значення;
