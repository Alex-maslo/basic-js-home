"use strict";

/* Завдання
Реалізувати функцію створення об'єкта "юзер".
Завдання має бути виконане на чистому Javascript
без використання бібліотек типу jQuery або React.

Технічні вимоги:
Написати функцію createNewUser(), яка буде створювати та
повертати об'єкт newUser.
При виклику функція повинна запитати ім'я та прізвище.
Використовуючи дані, введені юзером, створити об'єкт 
newUser з властивостями firstName та lastName.
Додати в об'єкт newUser метод getLogin(), який повертатиме
першу літеру імені юзера, з'єднану з прізвищем, все в 
нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
Створити юзера за допомогою функції createNewUser().
Викликати у цього юзера функцію getLogin().
Вивести у консоль результат виконання функції.

Необов'язкове завдання підвищеної складності
Зробити так, щоб властивості firstName та lastName не можна
було змінювати напряму. Створити функції-сеттери setFirstName()
та setLastName(), які дозволять змінити дані властивості.*/

function createNewUser() {
  const newUser = {
    firstName: prompt("Введіть ім'я:", ""),
    lastName: prompt("Введіть прізвище:", ""),
    birthday: new Date(prompt("Введіть дату народження:", "Рік.Місяць.Число")),

    getLogin() {
      return (this.firstName.slice(0, 1) + this.lastName).toLowerCase();
    },

    getAge() {
      if (this.birthday.getMonth() > new Date().getMonth()) {
        return new Date().getFullYear() - this.birthday.getFullYear() - 1;
      } else {
        return new Date().getFullYear() - this.birthday.getFullYear();
      }
    },
    getPassword() {
      return (
        this.firstName[0].toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthday.getFullYear()
      );
    },
  };
  return newUser;
}

let user = createNewUser();

console.log(user);
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());
