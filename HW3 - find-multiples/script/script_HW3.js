"use strict";

/* Реалізувати програму на Javascript, яка знаходитиме всі
числа кратні 5 (діляться на 5 без залишку) у заданому діапазоні.
Завдання має бути виконане на чистому Javascript без використання
бібліотек типу jQuery або React.

Технічні вимоги:
Отримати за допомогою модального вікна браузера число, яке введе користувач.
Вивести в консолі всі числа, кратні 5, від 0 до введеного користувачем числа.
Якщо таких чисел немає - вивести в консоль фразу "Sorry, no numbers"
Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.*/

let userNumber;

do {
  userNumber = +prompt("Enter the number");
  if (isNaN(userNumber) == true) {
    alert("You need enter the number");
  }
} while (isNaN(userNumber) == true);

if (userNumber < 5) {
  alert("Sorry, no numbers");
} else {
  for (let i = 0; i <= userNumber; i++) {
    if (i % 5 == 0) {
      console.log(i);
    }
  }
}
